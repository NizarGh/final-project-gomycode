import React from "react";
import { Provider } from "react-redux";
import {
  createStackNavigator,
  createBottomTabNavigator,
  createDrawerNavigator,
  createAppContainer
} from "react-navigation";
import store from "./src/store";
import IconWithBadge from "./screenStyle";
import NavigateScreen from "./src/Screens/Navigate";
import Home from "./src/Screens/Home";
import AccountScreen from "./src/Screens/Account";
import BookScreen from "./src/Screens/Book";
import SidebarScreen from "./src/Screens/Sidebar";
// import ModalScreen from "./src/Screens/EnregistrationsScreen/SignUp";

const bottomTabIcons = {
  NavigateScreen: {
    text: "Navigate",
    type: "Entypo",
    name: "map"
  },

  HomeScreen: {
    text: "Home",
    type: "AntDesign",
    name: "home"
  },
  DrawerScreen: {
    text: "Account",
    type: "MaterialIcons",
    name: "person-outline"
  }
};

const RootStack = createBottomTabNavigator(
  {
    HomeScreen: createStackNavigator({
      Home,
      BookScreen
    }),
    NavigateScreen,
    DrawerScreen: createDrawerNavigator(
      {
        AccountScreen,
        SidebarScreen
      },
      {
        drawerPosition: "left"
        //drawerBackgroundColor: "grey"
      }
    )
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        let { routeName } = navigation.state;
        let { type, name, text } = bottomTabIcons[routeName];

        return (
          <IconWithBadge
            text={text.toUpperCase()}
            type={type}
            name={name}
            style={{
              color: tintColor,
              fontSize: focused ? 28 : 20
            }}
          />
        );
      }
    }),
    tabBarOptions: {
      activeTintColor: "white",
      //inactiveTintColor: "#5B3000",
      inactiveTintColor: "#0B2027",
      showLabel: false,
      style: {
        backgroundColor: "#94B9AF"
      }
    }
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}
