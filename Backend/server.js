const express = require("express");
const bodyParser = require("body-parser");
const { MongoClient, ObjectID } = require("mongodb");
const assert = require("assert");

const app = express();
app.use(bodyParser.json());

const MongoUrl = "mongodb://localhost:27017";
const dataBase = "Easypark";

MongoClient.connect(MongoUrl, { useNewUrlParser: true }, (err, client) => {
  assert.equal(err, null, "Database connection failed");
  const db = client.db(dataBase);

  app.post("/add-parking", (req, res) => {
    let newPark = req.body;
    db.collection("Parkings").insertOne(newPark, (err, data) => {
      if (err) {
        res.send("can not add new Parking");
      } else res.send("new parking added");
    });
  });

  app.get("/parking-list", (req, res) => {
    db.collection("Parkings")
      .find()
      .toArray((err, data) => {
        if (err) {
          res.send("can not get Parkings list");
        } else res.send(data);
      });
  });

  app.post("/user-identifier", (req, res) => {
    let loggedUser = req.body;
    db.collection("Users")
      .findOne({
        $and: [
          { username: loggedUser.username },
          { password: loggedUser.password }
        ]
      })
      .then(user => {
        if (!user) {
          return res.send("not registred");
        }
        res.send(user);
      })
      .catch(err => res.send(err));
  });

  app.post("/bookable-parking-list", (req, res) => {
    let loggedUser = req.body;
    db.collection("Parkings")
      .find({
        region: loggedUser.region,
        bookable: true
      })
      .toArray((err, data) => {
        if (err) {
          res.send("no parking available");
        } else res.send(data);
      });
  });

  app.post("/update-reservation", (req, res) => {
    let { user } = req.body;
    db.collection("Users")
      .findOneAndUpdate(
        { username: user.name },
        { $push: { reservations: user.reservation } }
      )
      .then(user => {
        return res.send(user);
      })
      .catch(err);
  });
});

const port = 3100;
app.listen(port, err => {
  if (err) console.log("server error");
  else console.log(`server is running on ${port}`);
});
