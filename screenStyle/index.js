import React, { Component } from "react";

// Re-usable component
import { View, Text } from "react-native";
import { Icon } from "native-base";

// Utils
import styles from "./style";

class IconWithBadge extends Component {
  render() {
    return (
      <View style={styles.iconContainer}>
        <Icon
          name={this.props.name}
          type={this.props.type}
          style={this.props.style}
        />
        <Text style={{ ...styles.text, color: this.props.style.color }}>
          {this.props.text}
        </Text>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return { items: state.cart.items };
};

export default IconWithBadge;
