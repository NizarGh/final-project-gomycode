export default {
  iconContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    fontSize: 9,
    fontWeight: "bold"
  }
};
