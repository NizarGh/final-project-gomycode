import React from "react";
import { Button, Icon, Fab } from "native-base";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false
    };
  }

  render() {
    return (
      <Fab
        active={this.state.active}
        direction="up"
        containerStyle={{}}
        style={{
          //backgroundColor: "#5067FF"
          backgroundColor: "#3B5998"
        }}
        position="bottomRight"
        onPress={() =>
          this.setState(
            {
              active: !this.state.active
            },
            this.props.parkingDisplay ? this.props.addFunc : null
          )
        }
      >
        <Icon type="Entypo" name="compass" />
        <Button
          style={{ backgroundColor: "#40798C" }}
          disabled={this.props.disabled}
          onPress={() => {
            alert("pressed");
            this.props.displayWay;
          }}
        >
          <Icon type="Entypo" name="compass" />
          {/* <Image source={require("./mapWay.png")} /> */}
        </Button>
        <Button
          style={{ backgroundColor: "#DD5144" }}
          onPress={this.props.addFunc}
        >
          <Icon type="MaterialIcons" name="add-location" />
        </Button>
      </Fab>
    );
  }
}
