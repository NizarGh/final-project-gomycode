import React, { Component } from "react";
import { Footer, FooterTab, Button, Icon, Text, Badge } from "native-base";

export default class FooterComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }
  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
    });
    this.setState({ loading: false });
  }
  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }
    return (
      <Footer>
        <FooterTab>
          <Button
            active={this.props.active === "home" ? true : false}
            vertical
            onPress={this.props.home}
          >
            <Icon name="home" />
            <Text>Home</Text>
          </Button>
          <Button
            active={this.props.active === "navigate" ? true : false}
            vertical
            onPress={this.props.navigate}
          >
            <Icon active name="navigate" />
            <Text>Navigate</Text>
          </Button>
          <Button
            active={this.props.active === "account" ? true : false}
            vertical
            onPress={this.props.account}
          >
            <Icon active name="person" />
            <Text>Account</Text>
          </Button>
          {/* <Button badge active={this.props.active==="reservations" ? true:false} vertical onPress={() => {
              this.props.reservations(), this.setState({
                active: "reservations"
              });
            }}>
            <Badge>
              <Text>51</Text>
            </Badge>
            <Icon name="book" />
            <Text>Reservations</Text>
          </Button> */}
        </FooterTab>
      </Footer>
    );
  }
}
