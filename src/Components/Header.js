import React, { Component } from "react";
import { Header, Left, Body, Right, Title } from "native-base";
export default class HeaderComponent extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
    });
    this.setState({ loading: false });
  }
  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }
    return (
      <Header
        style={{
          //backgroundColor: "#82A6B1"
          backgroundColor: "#94B9AF",
          alignItems: "center"
        }}
      >
        <Left>
          {/* <Button transparent>
            <Icon name="menu" />
          </Button> */}
        </Left>
        <Body>
          <Title>{this.props.text}</Title>
        </Body>
        <Right>
          {/* <Button transparent onPress={this.props.screen}>
            <Icon name="arrow-back" />
          </Button> */}
        </Right>
      </Header>
    );
  }
}
