import React, { Component } from "react";
import { Text, TouchableOpacity, StyleSheet, View } from "react-native";
import { Input, Form, Item } from "native-base";
import Modal from "react-native-modal";

export default class Modal2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleModal: false
    };
  }

  onHandle = () => {
    if (this.props.visibleModal === 2) {
      this.setState({ visibleModal: true });
      return true;
    }
  };

  _renderButton = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.button}>
        <Text>{text}</Text>
      </View>
    </TouchableOpacity>
  );

  _renderModalContent = () => (
    <View style={styles.modalContent}>
      <Form>
        {this.props.inputs.map((el, i) => {
          return (
            <Item style={styles.item}>
              <Input
                placeholder={el.title}
                value={this.state.username}
                onChangeText={val => this.setState({ username: val })}
              />
            </Item>
          );
        })}
      </Form>

      {this.props.texts.map((el, i) => {
        return (
          <View>
            <Text>{el.text}</Text>
          </View>
        );
      })}

      {this.props.buttons.map((el, i) => {
        {
          () =>
            this._renderButton(el.title, () => {
              this.setState({ visibleModal: false });
              el.func;
            });
        }
      })}
    </View>
  );

  render() {
    return (
      <View style={styles.container}>
        <Modal isVisible={() => this.onHandle()}>
          {this._renderModalContent()}
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  item: {
    width: "100%"
  },
  button: {
    backgroundColor: "lightblue",
    padding: 12,
    margin: 16,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  modalContent: {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  bottomModal: {
    justifyContent: "flex-end",
    margin: 0
  }
});
