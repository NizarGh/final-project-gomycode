import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import { StyleSheet } from "react-native";

export default class DateTimePickerTester extends Component {
  state = {
    isDateTimePickerVisible: false,
    selectedDate: ""
  };

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = date => {
    this.setState({ selectedDate: date.toString() }, () =>
      this.props.func(this.state.selectedDate, this.props.title)
    );
    this._hideDateTimePicker();
  };

  render() {
    const { isDateTimePickerVisible, selectedDate } = this.state;

    return (
      <View style={this.props.styles.container}>
        <TouchableOpacity onPress={this._showDateTimePicker}>
          <View style={this.props.styles.button}>
            <Text>{this.props.title}</Text>
          </View>
        </TouchableOpacity>

        {/* <Text style={this.props.styles.text}>{selectedDate}</Text> */}

        <DateTimePicker
          isVisible={isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
          mode={"datetime"}
          datePickerModeAndroid={"spinner"}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    backgroundColor: "lightblue",
    padding: 12,
    margin: 16,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  text: {
    marginVertical: 10
  }
});
