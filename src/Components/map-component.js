import React from "react";
import axios from "axios";
import {
  Image,
  StyleSheet,
  ToastAndroid,
  TouchableOpacity
} from "react-native";
import { View } from "native-base";
import { Marker } from "react-native-maps";
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import { Location, Permissions } from "expo";

import FabComponent from "./Fab";
import mapStyle from "../Style/mapStyle";
//import { getTimeFieldValues } from "uuid-js";

const bottom = "50%";
const right = "45%";

export default class MapComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      parkings: [
        {
          verified: true,
          coordinate: { latitude: 36.835466, longitude: 10.242494 }
        },
        {
          verified: true,
          coordinate: { latitude: 36.833524, longitude: 10.240098 }
        }
      ],
      newParking: {},
      marginTop: 0,
      coordinate: { latitude: 36.835464, longitude: 10.242534 },
      addParkingDisplay: false,
      active: false,
      disabled: true
    };
  }

  async componentDidMount() {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      ToastAndroid.show("A pikachu appeared nearby !", ToastAndroid.SHORT);
    }
    let {
      coords: { longitude, latitude }
    } = await Location.getCurrentPositionAsync();

    this.setState({
      coordinate: {
        latitude: latitude,
        longitude: longitude
      }
    });

    axios
      .get("http://192.168.6.124:3100/parking-list")
      .then(res =>
        this.setState({
          parkings: res.data
        })
      )
      .catch(err => alert(err));
  }

  onPressFunc = () => {
    console.log(this.state.parkings[0].coordinate);

    alert(this.state.parkings[0]);

    // axios
    //   .post("http://192.168.6.124:3100/add-parking", this.state.parkings)
    //   .then(res => alert(res.data))
    //   .catch(err => alert(err));
  };

  onMapReadyFunc = () => {
    axios
      .get("http://192.168.6.124:3100/parking-list")
      .then(res =>
        this.setState({
          marginTop: 22,
          parkings: res.data
        })
      )
      .catch(err => alert(err));
  };

  onSelectMarker = () => {
    this.setState({
      disabled: false
    });
  };

  onAddParkingDisplay = () => {
    let parkDisplay = this.state.addParkingDisplay;
    this.setState({
      addParkingDisplay: parkDisplay ? false : true
    });
  };

  onAddParking = () => {
    let parkDisplay = this.state.addParkingDisplay;
    this.setState({
      addParkingDisplay: parkDisplay ? false : true,
      parkings: parkDisplay
        ? [...this.state.parkings, this.state.newParking]
        : this.state.parkings
    });
  };

  // onWayPress = () => {};

  render() {
    return (
      <View style={{ flex: 1 }}>
        <MapView
          loadingEnabled
          style={{
            flex: 1,
            marginTop: this.state.marginTop
          }}
          customMapStyle={mapStyle}
          initialRegion={{
            latitude: this.state.coordinate.latitude,
            longitude: this.state.coordinate.longitude,
            latitudeDelta: 0.02,
            longitudeDelta: 0.02
          }}
          showsMyLocationButton
          showsUserLocation
          followsUserLocation
          rotateEnabled
          onMapReady={() => {
            this.setState({ marginTop: 24 });
          }}
          onPress={() =>
            this.setState({
              disabled: true
            })
          }
          onRegionChangeComplete={region => {
            this.setState({
              newParking: {
                verified: false,
                coordinate: {
                  latitude: region.latitude,
                  longitude: region.longitude
                }
              }
            });
          }}
        >
          {this.state.parkings.map((e, i) => {
            return (
              <Marker
                key={i}
                coordinate={e.coordinate}
                title="Parking"
                pinColor={e.verified ? "red" : "blue"}
                description={e.type + " / " + e.streetName}
                onPress={() => this.onSelectMarker()}
                //onPress={e => console.log(e.nativeEvent)}
              />
            );
          })}

          {/* <MapViewDirections
            origin={{ latitude: 37.3317876, longitude: -122.0054812 }}
            destination={{ latitude: 37.3317876, longitude: -121.0054812 }}
            apikey={"AIzaSyDN6qK_jeHt87dBt5Le7GWEHT9SjYNzvXY"}
            strokeWidth={3}
            strokeColor="hotpink"
            optimizeWaypoints
          /> */}
        </MapView>

        <FabComponent
          disabled={this.state.disabled}
          addFunc={() => this.onAddParkingDisplay()}
          displayWay={() => this.onWayPress()}
          parkingDisplay={this.state.addParkingDisplay}
        />

        <TouchableOpacity
          style={styles.parkingIcon}
          onPress={() => this.onAddParking()}
        >
          <Image
            style={{
              ...styles.imgParking,
              display: this.state.addParkingDisplay ? "flex" : "none"
            }}
            source={{
              uri:
                "https://cdn2.iconfinder.com/data/icons/location-map-simplicity/512/parking-512.png"
            }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  imgParking: {
    width: 50,
    height: 50
  },
  parkingIcon: {
    position: "absolute",
    bottom: bottom,
    right: right,
    justifyContent: "center",
    alignItems: "center"
  }
});
