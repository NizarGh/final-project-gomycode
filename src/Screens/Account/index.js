import React from "react";
import { connect } from "react-redux";
import store from "../../store";
import { Text, StatusBar, TouchableOpacity } from "react-native";
import {
  Container,
  Content,
  List,
  ListItem,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Badge
} from "native-base";
import HeaderComponent from "../../Components/Header";
import styles from "./styles";
import { auth_verif } from "../../actions";

class AccountScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: { username: "nizar12", password: "1234" },
      informations: "",
      reservationBadge: "flex",
      reservationNbr: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <Container style={{ paddingTop: StatusBar.currentHeight }}>
        <HeaderComponent text="Account" />

        <Content
          contentContainerStyle={{
            flex: 1,
            margin: 20
          }}
        >
          <List style={styles.list}>
            <ListItem icon style={styles.ListItem}>
              <Left>
                <Button
                  style={{ backgroundColor: "#94B9AF" }}
                  onPress={() => {
                    //this.props.navigation.navigate("NavigateScreen")
                  }}
                >
                  <Icon active type="MaterialIcons" name="person-outline" />
                </Button>
              </Left>
              <Body>
                <TouchableOpacity
                  onPress={() => {
                    //this.props.navigation.navigate("NavigateScreen")
                  }}
                >
                  <Text style={styles.text}>My account</Text>
                </TouchableOpacity>
              </Body>
              <Right style={styles.Right}>
                <Text style={{ fontSize: 14, color: "gray" }}>
                  {this.props.username.firstName} {this.props.username.lastName}
                </Text>
              </Right>
            </ListItem>
            <ListItem icon style={styles.ListItem}>
              <Left>
                <Button
                  style={{ backgroundColor: "#94B9AF" }}
                  //onPress={() => this.props.navigation.navigate("BookScreen")}
                >
                  <Icon active type="AntDesign" name="car" />
                </Button>
              </Left>
              <Body>
                <TouchableOpacity onPress={() => {}}>
                  <Text style={styles.text}>My vehicules</Text>
                </TouchableOpacity>
              </Body>
              <Right style={styles.Right}>
                <Text style={{ fontSize: 14, color: "gray" }}>
                  {this.props.vehicules[this.props.vehicules.length - 1]}{" "}
                </Text>
              </Right>
            </ListItem>
            <ListItem icon style={styles.ListItem}>
              <Left>
                <Button
                  style={{ backgroundColor: "#94B9AF" }}
                  onPress={
                    () => {}
                    //this.props.navigation.navigate("AccountScreen")
                  }
                >
                  <Icon active type="FontAwesome" name="bookmark-o" />
                </Button>
              </Left>
              <Body>
                <TouchableOpacity
                  onPress={() => {
                    console.log(store.getState());
                    console.log(this.props.reservations);
                    this.setState({ visibleModal: 1 });
                    this.props.auth_verif({});
                  }}
                >
                  <Text style={styles.text}>My reservations </Text>
                </TouchableOpacity>
              </Body>
              <Right
                style={{
                  width: 100,
                  height: 72.5,
                  display:
                    this.props.verified && this.props.reservations.length > 0
                      ? "flex"
                      : "none"
                }}
              >
                <Badge danger>
                  <Text>{this.props.reservations.length}</Text>
                </Badge>
              </Right>
            </ListItem>
            <ListItem icon style={styles.ListItem}>
              <Left>
                <Button
                  style={{ backgroundColor: "#94B9AF" }}
                  onPress={
                    () => {}
                    //this.props.navigation.navigate("AccountScreen")
                  }
                >
                  <Icon active type="SimpleLineIcons" name="present" />
                </Button>
              </Left>
              <Body>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ visibleModal: 1 });
                  }}
                >
                  <Text style={styles.text}>My promotional codes</Text>
                </TouchableOpacity>
              </Body>
              <Right
                style={{
                  width: 100,
                  height: 72.5,
                  display:
                    this.props.verified && this.props.promo_code.length > 0
                      ? "flex"
                      : "none"
                }}
              >
                <Badge danger>
                  <Text>
                    {this.props.verified ? this.props.reservations.length : "0"}
                  </Text>
                </Badge>
              </Right>
            </ListItem>
          </List>
          <Text>{this.props.reservations.toString()}</Text>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    verified: state.auth.verified,
    username: state.auth.user_name,
    reservations: state.reservations,
    vehicules: state.vehicules,
    promo_code: state.codes
  };
};

export default connect(
  mapStateToProps,
  { auth_verif }
)(AccountScreen);
