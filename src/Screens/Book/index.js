import React from "react";
import { connect } from "react-redux";
import axios from "axios";
import { View, Text, TouchableOpacity, StatusBar } from "react-native";
// import DateTimePicker from "react-native-modal-datetime-picker";
import {
  Container,
  Content,
  Item,
  Input,
  Right,
  Icon,
  ListItem,
  List,
  Body
} from "native-base";
import HeaderComponent from "../../Components/Header";
import DateTime from "../../Components/TimePicker";
import Modal from "react-native-modal";
import { add_reservations } from "../../actions";
import styles from "./styles";

class BookScreen extends React.Component {
  static navigationOptions = {
    headerTransparent: true,
    headerTintColor: "white"
  };

  state = {
    visibleModal: null,
    dateFrom: "",
    dateTo: "",
    search: "",
    parkingList: [
      {
        type: "Private parking",
        streetName: "Rue du Lac Nizar",
        price: "1",
        id: "13"
      }
    ]
  };

  reservation_Func = () => {
    // POST a new reservation for this client to the database
    this.setState({ visibleModal: null });
    this.props.add_reservations({
      id: this.state.parkingID,
      dateFrom: this.state.dateFrom,
      dateTo: this.state.dateTo
    });
    this.props.navigation.navigate("HomeScreen");
    axios
      .post("http://192.168.6.124:3100/update-reservation", {
        user: {
          name: "nizar12",
          reservation: {
            id: this.state.parkingID,
            dateFrom: this.state.dateFrom,
            dateTo: this.state.dateTo
          }
        }
      })
      .then(res => {
        this.props.add_reservations(res.data);
      })
      .catch(err => {
        alert(err);
      });
  };

  _renderButton = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.buttonModal}>
        <Text>{text}</Text>
      </View>
    </TouchableOpacity>
  );

  _renderModalContent = () => (
    <View style={styles.modalContent}>
      <Text>
        You can confirm your reservation by clicking on the Ok button !{" \n "}
        {this.state.dateFrom}
        {" \n "}
        {this.state.dateTo}
      </Text>
      <View style={{ display: "flex", flexDirection: "row", height: 100 }}>
        {this._renderButton("Ok", () => this.reservation_Func())}
        {this._renderButton("Cancel", () => {
          this.setState({ visibleModal: null });
        })}
      </View>
    </View>
  );

  changeHandle = val => {
    this.setState({
      search: val
    });
  };

  onSubmitInput = () => {
    axios
      .post("http://192.168.6.124:3100/bookable-parking-list", {
        region: this.state.search.toLowerCase()
      })
      .then(res => {
        this.setState({
          parkingList: res.data
        });
      })
      .catch(err => alert(err));
  };

  getTimeDate = (date, fromTo) => {
    if (fromTo === "From") this.setState({ dateFrom: date });
    else if (fromTo === "To") this.setState({ dateTo: date });
  };

  render() {
    return (
      <Container style={{ paddingTop: StatusBar.currentHeight }}>
        <HeaderComponent text="Booking page" />
        <Content
          contentContainerStyle={{
            flex: 1,
            margin: 30
          }}
        >
          <Container>
            <Item regular>
              <Input
                value={this.state.search}
                onSubmitEditing={() => this.onSubmitInput()}
                onChangeText={val => this.changeHandle(val)}
                placeholder="Where do you want to go"
              />
              <Icon active name="search" />
            </Item>
            <Container style={styles.content}>
              <DateTime
                title="From"
                styles={styles}
                func={(date, fromTo) => this.getTimeDate(date, fromTo)}
              />
              <DateTime
                title="To"
                styles={styles}
                func={(date, fromTo) => this.getTimeDate(date, fromTo)}
              />
            </Container>
            <Container style={{ flex: 5 }}>
              <List style={styles.list}>
                {this.state.parkingList.map((el, i) => {
                  return (
                    <ListItem key={i}>
                      <Body>
                        <TouchableOpacity
                          onPress={() => {
                            this.setState({
                              visibleModal: 1,
                              parkindID: el.id
                            });
                          }}
                        >
                          <Text style={{ fontSize: 12, margin: 0, padding: 0 }}>
                            {el.type}
                          </Text>
                          <Text style={{ fontSize: 10, margin: 0, padding: 0 }}>
                            {el.streetName}
                          </Text>
                          <Modal isVisible={this.state.visibleModal === 1}>
                            {this._renderModalContent()}
                          </Modal>
                        </TouchableOpacity>
                      </Body>
                      <Right>
                        <Text style={{ textAlign: "right" }}>
                          {el.price}DT/h
                        </Text>
                      </Right>
                    </ListItem>
                  );
                })}
              </List>
            </Container>
          </Container>
        </Content>
      </Container>
    );
  }
}
const mapStateToProps = state => ({
  reservations: state.reservations
});
export default connect(
  mapStateToProps,
  { add_reservations }
)(BookScreen);
