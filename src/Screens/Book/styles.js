import { StyleSheet } from "react-native";

export default StyleSheet.create({
  content: {
    marginTop: 5,
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
    height: 100
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: 100,
    height: 70
  },
  button: {
    backgroundColor: "#9DB5B2",
    padding: 12,
    margin: 16,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)",
    width: 150,
    height: 40
  },
  text: {
    marginVertical: 10
  },
  buttonModal: {
    backgroundColor: "lightblue",
    padding: 12,
    margin: 16,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  modalContent: {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  }
});
