import { StyleSheet } from "react-native";
import { StatusBar } from "react-native";

export default StyleSheet.create({
  map: { marginTop: StatusBar.currentHeight }
});
