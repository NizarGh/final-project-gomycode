import React from "react";
import { Text, StatusBar } from "react-native";
import { Container, Content } from "native-base";
import HeaderComponent from "../Components/Header";

export default class SidebarScreen extends React.Component {
  static navigationOptions = {
    headerTransparent: true,
    headerMode: "float",
    headerTintColor: "white"
  };
  render() {
    return (
      <Container style={{ paddingTop: StatusBar.currentHeight }}>
        <HeaderComponent
          sidebar={() => this.props.navigation.navigate("Sidebar")}
        />
        <Content
          contentContainerStyle={{
            flex: 1
          }}
        >
          <Text>Side Bar BABY !!</Text>
        </Content>
      </Container>
    );
  }
}
