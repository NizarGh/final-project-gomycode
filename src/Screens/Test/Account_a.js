import React from "react";
import { connect } from "react-redux";
import axios from "axios";
import store from "../../store";
import { Text, StatusBar, StyleSheet, TouchableOpacity } from "react-native";
import {
  Container,
  Content,
  List,
  ListItem,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Badge
} from "native-base";
import HeaderComponent from "../../Components/Header";

class AccountScreen extends React.Component {
  state = {
    user: { username: "nizar12", password: "1234" },
    informations: "",
    reservationBadge: "flex",
    reservationNbr: ""
  };

  static navigationOptions = {
    header: null
  };

  // componentDidMount() {
  //   axios
  //     .post("http://192.168.6.124:3100/user-identifier", this.state.user)
  //     .then(res => {
  //       if (res.data === "not registred") {
  //         alert(message);
  //       } else {
  //         this.setState({
  //           informations: res.data,
  //           loaded: true
  //         });
  //       }
  //     })
  //     .catch(err => {
  //       alert(err);
  //     });
  // }

  dataArray = [
    { title: "My account", content: "Lorem ipsum dolor sit amet" },
    { title: "My vehicules", content: "Lorem ipsum dolor sit amet" },
    { title: "My reservations", content: "Lorem ipsum dolor sit amet" },
    { title: "My promotional codes", content: "Lorem ipsum dolor sit amet" }
  ];

  render() {
    return (
      <Container style={{ paddingTop: StatusBar.currentHeight }}>
        <HeaderComponent
          text="Account"
          // sidebar={() => this.props.navigation.navigate("Sidebar")}
        />

        <Content
          contentContainerStyle={{
            flex: 1,
            margin: 20
          }}
        >
          {/* <Accordion
            dataArray={this.dataArray}
            iconStyle={{ color: "green" }}
            expandedIconStyle={{ color: "red" }}
          /> */}
          <List style={styles.list}>
            <ListItem icon style={styles.ListItem}>
              <Left>
                <Button
                  style={{ backgroundColor: "#94B9AF" }}
                  onPress={() => {
                    //this.props.navigation.navigate("NavigateScreen")
                  }}
                >
                  <Icon active type="MaterialIcons" name="person-outline" />
                </Button>
              </Left>
              <Body>
                <TouchableOpacity
                  onPress={() => {
                    //this.props.navigation.navigate("NavigateScreen")
                  }}
                >
                  <Text style={styles.text}>My account</Text>
                </TouchableOpacity>
              </Body>
              <Right style={styles.Right}>
                <Text style={{ fontSize: 14, color: "gray" }}>
                  {this.props.username.firstName} {this.props.username.lastName}
                </Text>
              </Right>
            </ListItem>
            <ListItem icon style={styles.ListItem}>
              <Left>
                <Button
                  style={{ backgroundColor: "#94B9AF" }}
                  //onPress={() => this.props.navigation.navigate("BookScreen")}
                >
                  <Icon active type="AntDesign" name="car" />
                </Button>
              </Left>
              <Body>
                <TouchableOpacity onPress={() => {}}>
                  <Text style={styles.text}>My vehicules</Text>
                </TouchableOpacity>
              </Body>
              <Right style={styles.Right}>
                <Text style={{ fontSize: 14, color: "gray" }}>
                  {this.props.vehicules[this.props.vehicules.length - 1]}{" "}
                </Text>
              </Right>
            </ListItem>
            <ListItem icon style={styles.ListItem}>
              <Left>
                <Button
                  style={{ backgroundColor: "#94B9AF" }}
                  onPress={
                    () => {}
                    //this.props.navigation.navigate("AccountScreen")
                  }
                >
                  <Icon active type="FontAwesome" name="bookmark-o" />
                </Button>
              </Left>
              <Body>
                <TouchableOpacity
                  onPress={() => {
                    console.log(store.getState());
                    console.log(this.props.reservations);
                    this.setState({ visibleModal: 1 });
                  }}
                >
                  <Text style={styles.text}>My reservations </Text>
                </TouchableOpacity>
              </Body>
              <Right
                style={{
                  width: 100,
                  height: 72.5,
                  display:
                    this.props.verified && this.props.reservations.length > 0
                      ? "flex"
                      : "none"
                }}
              >
                <Badge danger>
                  <Text>{this.props.reservations.length}</Text>
                </Badge>
              </Right>
            </ListItem>
            <ListItem icon style={styles.ListItem}>
              <Left>
                <Button
                  style={{ backgroundColor: "#94B9AF" }}
                  onPress={
                    () => {}
                    //this.props.navigation.navigate("AccountScreen")
                  }
                >
                  <Icon active type="SimpleLineIcons" name="present" />
                </Button>
              </Left>
              <Body>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ visibleModal: 1 });
                  }}
                >
                  <Text style={styles.text}>My promotional codes</Text>
                </TouchableOpacity>
              </Body>
              <Right
                style={{
                  width: 100,
                  height: 72.5,
                  display:
                    this.props.verified && this.props.promo_code.length > 0
                      ? "flex"
                      : "none"
                }}
              >
                <Badge danger>
                  <Text>
                    {this.props.verified ? this.props.reservations.length : "0"}
                  </Text>
                </Badge>
              </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    verified: state.auth.verified,
    username: state.auth.user_name,
    reservations: state.reservations,
    vehicules: state.vehicules,
    promo_code: state.codes
  };
};

// store.subscribe(mapStateToProps());

const styles = StyleSheet.create({
  list: {
    width: "100%"
  },
  ListItem: {
    height: 100
  },
  text: {
    fontSize: 18
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  item: {
    width: "100%"
  },
  button: {
    backgroundColor: "lightblue",
    padding: 12,
    margin: 16,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  modalContent: {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  bottomModal: {
    justifyContent: "flex-end",
    margin: 0
  },
  Right: {
    width: 200,
    height: 72.5,
    padding: 0,
    margin: 0
  }
});

export default connect(mapStateToProps)(AccountScreen);
