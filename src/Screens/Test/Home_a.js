import React from "react";
import axios from "axios";
import {
  Text,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  Image
} from "react-native";
import { createStackNavigator } from "react-navigation";
import Modal from "react-native-modal";
import {
  Container,
  Content,
  List,
  ListItem,
  Button,
  Left,
  Right,
  Body,
  Title,
  Icon,
  View,
  Input,
  Form,
  Item,
  Label
} from "native-base";
import HeaderComponent from "../../Components/Header";
import BookScreen from "../Book";

class Home extends React.Component {
  state = {
    visibleModal: null,
    verified: false,
    user: {
      username: "nizar12",
      password: "1234"
    }
  };

  static navigationOptions = {
    header: null
    // title: "Home",
    // headerStyle: {
    //   backgroundColor: "#94B9AF"
    // },
    // headerTintColor: "white"
  };

  _renderButton = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.button}>
        <Text>{text}</Text>
      </View>
    </TouchableOpacity>
  );

  _renderModalContent = () => (
    <View style={styles.modalContent}>
      <Text>Sign In !</Text>
      <Form>
        <Item style={styles.item} inlineLabel>
          <Label>Username</Label>
          <Input
            placeholder="EasyPark"
            value={this.state.user.username}
            onChangeText={val =>
              this.setState({ user: { ...this.state.user, username: val } })
            }
          />
        </Item>
        <Item style={styles.item} inlineLabel>
          <Label>Password</Label>
          <Input
            placeholder="*****"
            secureTextEntry={true}
            value={this.state.user.password}
            onChangeText={val =>
              this.setState({ user: { ...this.state.user, password: val } })
            }
          />
        </Item>
      </Form>

      <View style={{ display: "flex", flexDirection: "row", height: 100 }}>
        {this._renderButton("Cancel", () => {
          this.setState({ visibleModal: null });
        })}
        {this._renderButton("Sign In", () => {
          this.verificationFunc(
            "Verify your user name or your password",
            "Sign-In"
          );
          //this.setState({ visibleModal: null });
        })}
      </View>
    </View>
  );

  verificationFunc = (message, type) => {
    axios
      .post("http://192.168.6.124:3100/user-identifier", this.state.user)
      .then(res => {
        if (res.data === "not registred") {
          alert(message);
        } else {
          this.setState({
            verified: true
          });
        }
      })
      .then(() => {
        if (this.state.verified && type === "Booking") {
          this.props.navigation.navigate("BookScreen");
        } else if (this.state.verified && type === "Sign-In") {
          this.props.navigation.navigate("AccountScreen");
          this.setState({ visibleModal: null });
        }
      })
      .catch(err => {
        alert(err);
        console.log(err);
      });
  };

  render() {
    return (
      <Container
        style={{ paddingTop: StatusBar.currentHeight, position: "relative" }}
      >
        <HeaderComponent
          text={"Home page"}
          sidebar={() => this.props.navigation.navigate("Sidebar")}
        />
        <Image
          style={{
            marginTop: 70,
            flex: 1,
            alignSelf: "stretch",
            width: undefined,
            height: undefined
          }}
          source={{
            uri:
              "https://www.wpclipart.com/travel/US_Road_Signs/info/parking.png"
          }}
        />
        <Content
          contentContainerStyle={{
            flex: 1,
            margin: 20
          }}
        >
          <List style={styles.list}>
            <ListItem icon>
              <Left>
                <Button
                  style={{ backgroundColor: "#94B9AF" }}
                  onPress={() =>
                    this.props.navigation.navigate("NavigateScreen")
                  }
                >
                  <Icon active type="MaterialIcons" name="local-parking" />
                </Button>
              </Left>
              <Body>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate("NavigateScreen")
                  }
                >
                  <Text style={styles.text}>Find a parking near you</Text>
                </TouchableOpacity>
              </Body>
            </ListItem>
            <ListItem icon>
              <Left>
                <Button
                  style={{ backgroundColor: "#94B9AF" }}
                  onPress={() => this.props.navigation.navigate("BookScreen")}
                >
                  <Icon active type="FontAwesome" name="bookmark-o" />
                </Button>
              </Left>
              <Body>
                <TouchableOpacity
                  onPress={() => {
                    this.verificationFunc(
                      "You need to log in or create an account before booking a place",
                      "Booking"
                    );
                  }}
                >
                  <Text style={styles.text}>Book a parking place</Text>
                </TouchableOpacity>
              </Body>
            </ListItem>
            <ListItem icon>
              <Left>
                <Button
                  style={{ backgroundColor: "#94B9AF" }}
                  onPress={() =>
                    this.props.navigation.navigate("AccountScreen")
                  }
                >
                  <Icon active type="Octicons" name="sign-in" />
                </Button>
              </Left>
              <Body>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({ visibleModal: 1 });
                  }}
                >
                  <Text style={styles.text}>Sign in</Text>

                  <Modal
                    verification={() =>
                      this.verificationFunc(
                        "Verify your user name or your password",
                        "Sign-In"
                      )
                    }
                    isVisible={this.state.visibleModal === 1}
                  >
                    {this._renderModalContent()}
                  </Modal>
                </TouchableOpacity>
              </Body>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  list: {
    position: "absolute",
    bottom: 30,
    width: "100%"
  },
  text: {
    fontSize: 18
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  item: {
    width: "100%"
  },
  button: {
    backgroundColor: "lightblue",
    padding: 12,
    margin: 16,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  modalContent: {
    backgroundColor: "white",
    padding: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  bottomModal: {
    justifyContent: "flex-end",
    margin: 0
  }
});

export default createStackNavigator({
  Home,
  BookScreen
});
