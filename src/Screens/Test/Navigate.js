import React from "react";
import { View, StatusBar } from "react-native";
import { Container, Content } from "native-base";
import HeaderComponent from "../../Components/Header";
import MapComponent from "../../Components/map-component";

export default class NavigateScreen extends React.Component {
  static navigationOptions = {
    header: null,
    headerStyle: {
      backgroundColor: "#f4511e"
    }
  };
  render() {
    return (
      <Container style={{ marginTop: StatusBar.currentHeight }}>
        <HeaderComponent
          text="Navigate"
          screen={() => this.props.navigation.navigate("Home")}
          // sidebar={() => this.props.navigation.navigate("Sidebar")}
        />
        <Content
          contentContainerStyle={{
            flex: 1
          }}
        >
          <MapComponent />
        </Content>
      </Container>
    );
  }
}
