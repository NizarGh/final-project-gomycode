import React from "react";
import { View, Text, StatusBar } from "react-native";
import { Container, Content } from "native-base";
import HeaderComponent from "../../Components/Header";
import FooterComponent from "../../Components/Footer";

export default class ReservationsScreen extends React.Component {
  static navigationOptions = {
    header: null
  };
  render() {
    return (
      <Container style={{ paddingTop: StatusBar.currentHeight }}>
        <HeaderComponent
          sidebar={() => this.props.navigation.navigate("Sidebar")}
        />
        <Content
          contentContainerStyle={{
            flex: 1
          }}
        >
          <Text>Reservations !!</Text>
        </Content>
      </Container>
    );
  }
}
