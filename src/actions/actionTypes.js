// Reservation Actions
export const ADD_RESERVATION = "ADD_RESERVATION";
export const DEL_RESERVATION = "DEL_RESERVATION";

// Vehicule Actions
export const ADD_VEHICULE = "ADD_VEHICULE";
export const DEL_VEHICULE = "DEL_VEHICULE";

// Promo code Actions
export const ADD_PROMO = "ADD_PROMO";
export const DEL_PROMO = "DEL_PROMO";

// Auth Actions
export const SUCCESS_AUTH = "SUCCESS_AUTH";
