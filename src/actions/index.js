import {
  ADD_RESERVATION,
  DEL_RESERVATION,
  SUCCESS_AUTH,
  ADD_VEHICULE,
  DEL_VEHICULE,
  ADD_PROMO,
  DEL_PROMO
} from "./actionTypes";

// RESERVATION ACTIONS
export const add_reservations = value => ({
  type: ADD_RESERVATION,
  value
});

export const del_reservations = value => ({
  type: DEL_RESERVATION,
  value
});

// VEHICULE ACTIONS
export const add_vehicule = value => ({
  type: ADD_VEHICULE,
  value
});

export const del_vehicule = value => ({
  type: DEL_VEHICULE,
  value
});

// PROMO_CODE ACTIONS
export const add_promo = value => ({
  type: ADD_PROMO,
  value
});

export const del_promo = value => ({
  type: DEL_PROMO,
  value
});

// AUTHENTICATION ACTIONS
export const auth_verif = value => ({
  type: SUCCESS_AUTH,
  value
});
