import { SUCCESS_AUTH } from "../actions/actionTypes";

const INITIAL_STATE = {
  verified: true,
  user_name: { firstName: "Ghazouani", lastName: "nizar" }
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SUCCESS_AUTH:
      return { verified: true, user_name: action.value };
    default:
      return state;
  }
};
