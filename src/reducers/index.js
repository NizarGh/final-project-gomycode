import { combineReducers } from "redux";
import auth from "./auth";
import reservations from "./reservations";
import vehicules from "./vehicules";
import codes from "./promoCode";

export default combineReducers({
  auth,
  reservations,
  vehicules,
  codes
});
