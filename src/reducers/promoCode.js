import { ADD_PROMO, DEL_PROMO } from "../actions/actionTypes";

const INITIAL_STATE = [];

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_PROMO:
      return [...state, action.value];
    case DEL_PROMO:
      return state.filter(el => el !== action.value);
    default:
      return state;
  }
};
