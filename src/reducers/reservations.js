import { ADD_RESERVATION, DEL_RESERVATION } from "../actions/actionTypes";

const INITIAL_STATE = [];

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_RESERVATION:
      return [...state, action.value];
    case DEL_RESERVATION:
      return state.filter(el => el !== action.value);
    default:
      return state;
  }
};
