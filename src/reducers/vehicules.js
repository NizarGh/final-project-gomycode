import { ADD_VEHICULE, DEL_VEHICULE } from "../actions/actionTypes";

const INITIAL_STATE = ["161TU9997"];

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_VEHICULE:
      return [...state, action.value];
    case DEL_VEHICULE:
      return state.filter(el => el !== action.value);
    default:
      return state;
  }
};
